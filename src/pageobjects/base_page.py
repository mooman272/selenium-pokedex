class BasePageObject(object):
    '''
    Webpage elements will be represented by Page Objects as defined by these
    classes. This BasePageObject class is intended to be the superclass for
    all Webpage Page Objects. It is not intended to be instantiated directly. 
    '''

    def __init__(self, selenium_driver):
        self.driver = selenium_driver
    