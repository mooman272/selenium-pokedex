'''
This module contains Page Objects for the Pokedex.
'''

import time

from selenium.webdriver.common.action_chains import ActionChains

from pageobjects.base_page import *
from pageobjects.locators import locator

class Search(BasePageObject):
    '''
    The Search object applies to the search field and its functionality.
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    def search_for(self, query):
        '''
        Input:  <str> Set search to this value.
        Output: None.
        
        Sets search field to the given value and clicks on the magnifying
        glass. Yeah, I could just press enter, but hey.
        '''
        self.query = query
        # button may be covered by nav if scrolled down
        # this line scrolls to the top. not the most elegant solution, but will
        # work for demo purposes
        self.driver.execute_script("window.scrollTo(0,0)")
        self.driver.find_element(*locator['pokedex']['search']['button']).click()
        results_list = ResultsList(self.driver)
        results_list.wait_to_finish_loading(10)
        
    @property
    def query(self):
        '''
        Search field.
        
        When typing into the field, it is cleared before setting the value.
        Search itself is not executed. If a newline char was explicitly added
        to the query (which would otherwise execute the search), it will be
        stripped.
        '''
        search_input = self.driver.find_element(*locator['pokedex']['search']['field'])
        return search_input.get_attribute('value') 
    
    @query.setter
    def query(self, value):
        if value.endswith('\n'): value = value.strip('\n')
        search_input = self.driver.find_element(*locator['pokedex']['search']['field'])
        search_input.clear()
        search_input.send_keys(value)
        return
    
    def dropdown_is_displayed(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        return self.driver.find_element(*locator['pokedex']['search']['dropdown']).is_displayed()
    
    @property
    def suggestions(self):
        '''
        (Read-only) <list> of <str> representing each search suggestion.
        '''
        suggestion_element_list = self.driver.find_elements(*locator['pokedex']['search']['suggestion'])
        suggestion_list = []
        for suggestion_element in suggestion_element_list:
            if suggestion_element.is_displayed(): suggestion_list.append(suggestion_element.text)
        return suggestion_list
    
    def find_suggestion_element(self, query):
        '''
        Input:  <str> (case-insensitive)
        Output: <WebElement> corresponding to the query.
                <None> if no match found.
        '''
        suggestion_element_list = self.driver.find_elements(*locator['pokedex']['search']['suggestion'])
        for suggestion_element in suggestion_element_list:
            if suggestion_element.is_displayed() and suggestion_element.text.lower() == query.lower(): return suggestion_element
        return None
    
    def select_suggestion(self, suggestion_text):
        '''
        Input:  <str> (case-insensitive)
        Output: None.
        
        Executes a search by clicking on a suggestion and clicking the
        magnifying glass
        '''
        webelement = self.find_suggestion_element(suggestion_text)
        if webelement == None: raise Exception("No search suggestion found matching '{}'.".format(suggestion_text))
        webelement.click()
        self.driver.find_element(*locator['pokedex']['search']['button']).click()
        return
    
    def mouseover(self, target, suggestion_text=None):
        '''
        Input:  <str> Thing to mouseover.
                <str> (Required if target='suggestion') Suggestion's text.
        Output: None.
        '''
        if target.lower() == 'search_button':
            webelement = self.driver.find_element(*locator['pokedex']['search']['button'])
        elif target.lower() == 'suggestion':
            if suggestion_text == None: raise Exception('Attempting to mouse over a search suggestion, but no text was given.')
            webelement = self.find_suggestion_element(suggestion_text)
            if webelement == None: raise Exception("No search suggestion found matching '{}'.".format(suggestion_text))
        else:
            raise Exception('Invalid mouseover target given.')
        ActionChains(self.driver).move_to_element(webelement).perform()
        return
    
class AdvancedSearch(BasePageObject):
    '''
    The Advanced Search object applies to the advanced search feature, which
    is found under the search field and above the sort dropdown.
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    def expand(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        if self.is_expanded(): raise Exception('Advanced Search is already expanded.')
        self.driver.find_element(*locator['pokedex']['advanced_search']['toggle']).click()
        self.wait_for_expansion()
        return
    
    def collapse(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        if not self.is_expanded(): raise Exception('Advanced Search is already collapsed.')
        self.driver.find_element(*locator['pokedex']['advanced_search']['toggle']).click()
        self.wait_for_collapse()
        return
    
    def is_expanded(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['filters']).is_displayed()
    
    def wait_for_expansion(self, time_limit=5.0):
        """
        Inputs: <int/float> (Optional) Wait time limit.
        Output: None if successful. Exception otherwise.
        """
        end_time = time.time() + time_limit
        while time.time() < end_time:
            time.sleep(0.5)
            if self.is_expanded():
                return
        raise Exception('Timeout: Advanced Search did not expand.')
    
    def wait_for_collapse(self, time_limit=5.0):
        """
        Inputs: <int/float> (Optional) Wait time limit.
        Output: None if successful. Exception otherwise.
        """
        end_time = time.time() + time_limit
        while time.time() < end_time:
            time.sleep(0.5)
            if not self.is_expanded():
                return
        raise Exception('Timeout: Advanced Search did not collapse.')
    
    def find_element_with_tw_description(self, tw_description):
        '''
        Input:  <str> Specific Type/Weakness, case-insensitive (e.g. electric)
        Output: <WebElement> corresponding to input.
                <None> if input not found.
        '''
        if not self.is_expanded(): raise Exception('Advanced Search needs to be expanded before this method is called.')
        tw_element_list = self.driver.find_elements(*locator['pokedex']['advanced_search']['tw'])
        for tw_element in tw_element_list:
            if tw_element.find_element(*locator['pokedex']['tw_filter']['description']).text.lower() == tw_description.lower(): return tw_element
        return None
    
    def mouseover(self, target):
        '''
        Input:  <str> Thing to mouseover.
        Output: None.
        '''
        if target.lower() == 'search_button':
            webelement = self.driver.find_element(*locator['pokedex']['advanced_search']['search'])
        elif target.lower() == 'reset_button':
            webelement = self.driver.find_element(*locator['pokedex']['advanced_search']['reset'])
        else:
            raise Exception('Invalid mouseover target given.')
        ActionChains(self.driver).move_to_element(webelement).perform()
        return
    
class TypeWeaknessFilter(BasePageObject):
    '''
    The Type/Weakness Filters are located in Advanced Search.
    '''
    
    def __init__(self, webdriver, tw_description):
        '''
        Inputs: <WebDriver>
                <str> Specific Type/Weakness, case-insensitive (e.g. electric)
                
        Because there are multiple Type/Weakness Filters, one needs to be
        specified when this class is instantiated.
        '''
        super().__init__(webdriver)
        advanced_search = AdvancedSearch(self.driver)
        if not advanced_search.is_expanded(): raise Exception('Advanced Search needs to be expanded before this class is instantiated.')
        webelement = advanced_search.find_element_with_tw_description(tw_description.lower())
        if webelement == None: raise Exception("Type/Weakness filter '{}' not found.".format(tw_description))
        self.webelement = webelement
    
    @property
    def description(self):
        '''
        (Read-only) <str> of the Type/Weeakness filter
        '''
        return self.webelement.find_element(*locator['pokedex']['tw_filter']['description']).text
    
    @property
    def type_toggle(self):
        '''
        Valid values: <bool>
        
        Corresponds to the 'T' button next to each of the filters. 
        '''
        if self.webelement.find_element(*locator['pokedex']['tw_filter']['type_toggle']).get_attribute('class').find('active-filter') != -1: return True
        return False
        
    @type_toggle.setter
    def type_toggle(self, value):
        if not self.type_toggle == value: self.webelement.find_element(*locator['pokedex']['tw_filter']['type_toggle']).click()
        return
    
    @property
    def weakness_toggle(self):
        '''
        Valid values: <bool>
        
        Corresponds to the 'W' button next to each of the filters. 
        '''
        if self.webelement.find_element(*locator['pokedex']['tw_filter']['weakness_toggle']).get_attribute('class').find('active-filter') != -1: return True
        return False
        
    @weakness_toggle.setter
    def weakness_toggle(self, value):
        if not self.weakness_toggle == value: self.webelement.find_element(*locator['pokedex']['tw_filter']['weakness_toggle']).click()
        return
    
class HeightFilter(BasePageObject):
    '''
    The HeightFilter object applies to the set of three toggles which,
    unfortunately, are not labeled with text. >:(
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    @property
    def short_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['height_short'])
    
    @property
    def medium_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['height_medium'])
    
    @property
    def tall_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['height_tall'])
    
    @property
    def short(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.short_webelement.get_attribute('class')
    
    @short.setter
    def short(self, value):
        if not self.short == value: self.short_webelement.click()
        return 
        
    @property
    def medium(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.medium_webelement.get_attribute('class')
    
    @medium.setter
    def medium(self, value):
        if not self.medium == value: self.medium_webelement.click()
        return 
        
    @property
    def tall(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.tall_webelement.get_attribute('class')
    
    @tall.setter
    def tall(self, value):
        if not self.tall == value: self.tall_webelement.click()
        return 
        
class WeightFilter(BasePageObject):
    '''
    The WeightFilter object applies to the set of three toggles which,
    unfortunately, are not labeled with text. >:(
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    @property
    def light_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['weight_light'])
    
    @property
    def medium_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['weight_medium'])
    
    @property
    def heavy_webelement(self):
        '''
        (Read-only) <WebElement>
        '''
        return self.driver.find_element(*locator['pokedex']['advanced_search']['weight_heavy'])
    
    @property
    def light(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.light_webelement.get_attribute('class')
    
    @light.setter
    def light(self, value):
        if not self.light == value: self.light_webelement.click()
        return 
        
    @property
    def medium(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.medium_webelement.get_attribute('class')
    
    @medium.setter
    def medium(self, value):
        if not self.medium == value: self.medium_webelement.click()
        return 
        
    @property
    def heavy(self):
        '''
        Valid values: <bool>
        '''
        return 'highlight' in self.heavy_webelement.get_attribute('class')
    
    @heavy.setter
    def heavy(self, value):
        if not self.heavy == value: self.heavy_webelement.click()
        return 
        
class Sort(BasePageObject):
    '''
    The Sort object applies to the sort dropdown and its functionality.
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    @property
    def current_webelement(self):
        '''
        (Read-only) <WebElement> of the current Sort method. (Click this to
        open the dropdown.)
        
        Unfortunately I could not determine a locator that returns a unique
        WebElement. The best I could get is two matches. One always seems
        to be invisible, so we'll return the other one.
        '''
        element_list = self.driver.find_elements(*locator['pokedex']['sort']['button'])
        if len(element_list) != 2: raise Exception('{} dropdowns were found; expected 2. :(')
        if element_list[0].is_displayed() and element_list[1].is_displayed(): raise Exception('Two visible sort dropdowns found. :(')
        if element_list[0].is_displayed(): return element_list[0]
        if element_list[1].is_displayed(): return element_list[1]
        raise Exception('Both dropdown elements not visible. :(')
    
    @property
    def current_value(self):
        '''
        (Read-only) <str> Current sort value.
        '''
        return self.current_webelement.text
    
    def by(self, sort_option):
        '''
        Input:  <str> of search option (case-insensitive).
        Output: None.
        
        Executes a Sort method.
        '''
        
        self.driver.execute_script("window.scrollTo(0,0)")
        if not self.dropdown_is_displayed(): self.open_dropdown()
        webelement = self.find_element_with_option(sort_option.lower())
        if webelement == None: raise Exception("No WebElement found for sort option '{}'.".format(sort_option.lower()))
        webelement.click()
        results_list = ResultsList(self.driver)
        results_list.wait_to_finish_loading()
        return
    
    def dropdown_is_displayed(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        return self.driver.find_element(*locator['pokedex']['sort']['dropdown']).is_displayed()
    
    def wait_for_dropdown_to_open(self, time_limit=5.0):
        """
        Inputs: <int/float> (Optional) Wait time limit.
        Output: None if successful. Exception otherwise.
        """
        end_time = time.time() + time_limit
        while time.time() < end_time:
            time.sleep(0.5)
            if self.dropdown_is_displayed():
                return
        raise Exception('Timeout: dropdown did not open.')
        
    def wait_for_dropdown_to_close(self, time_limit=5.0):
        """
        Inputs: <int/float> (Optional) Wait time limit.
        Output: None if successful. Exception otherwise.
        """
        end_time = time.time() + time_limit
        while time.time() < end_time:
            time.sleep(0.5)
            if not self.dropdown_is_displayed():
                return
        raise Exception('Timeout: dropdown did not close.')
        
    def open_dropdown(self):
        '''
        Inputs: None.
        Output: None.
                Exception if dropdown is already open.
        '''
        if self.dropdown_is_displayed(): raise Exception('Dropdown already open.')
        self.current_webelement.click()
        self.wait_for_dropdown_to_open()
        return
    
    def close_dropdown(self):
        '''
        Inputs: None.
        Output: None.
                Exception if dropdown is already closed.
        '''
        if not self.dropdown_is_displayed(): raise Exception('Dropdown already closed.')
        self.current_webelement.click()
        self.wait_for_dropdown_to_close()
        return
    
    @property
    def dropdown_options_elements(self):
        '''
        (Read-only) <list> of <WebElements> corresponding to each option in
        the sort dropdown.
                
        This can be called whether the dropdown is open or closed, as the
        WebElements exist either way.
        '''
        return self.driver.find_element(*locator['pokedex']['sort']['dropdown']).find_elements(*locator['pokedex']['sort']['dropdown_options'])
    
    def find_element_with_option(self, query):
        '''
        Input:  <str> of search option (case-insensitive).
        Output: <WebElement> corresponding to input <str>.
                <None> if no match found.
                
        At this time, the 'Sort results by...' option is using an _ellipsis,_
        not three periods. chr(8230) returns the corresponding string/char.
        '''
        if not self.dropdown_is_displayed(): raise Exception('Unable to search through the dropdown when it is closed.')
        for element in self.dropdown_options_elements:
            if query.lower() == element.text.lower(): return element
        return None
    
    def verify_sort(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        results_list = ResultsList(self.driver)
        if self.current_value == 'Lowest Number (First)':
            actual_list = results_list.all_numbers
            expected_list = actual_list.copy()
            expected_list.sort()
        elif self.current_value == 'Highest Number (First)':
            actual_list = results_list.all_numbers
            expected_list = actual_list.copy()
            expected_list.sort()
            expected_list.reverse()
        elif self.current_value == 'A-Z':
            actual_list = results_list.all_names
            expected_list = actual_list.copy()
            expected_list.sort()
        elif self.current_value == 'Z-A':
            actual_list = results_list.all_names
            expected_list = actual_list.copy()
            expected_list.sort()
            expected_list.reverse()
        elif self.current_value == 'Sort results by…':
            raise Exception('Select a valid sort method in the dropdown.')
        return actual_list == expected_list
    
    def mouseover(self, target):
        '''
        Input:  <str> Thing to mouseover.
        Output: None.
        '''
        if target.lower() == 'search_button':
            webelement = self.driver.find_element(*locator['pokedex']['advanced_search']['search'])
        elif target.lower() == 'reset_button':
            webelement = self.driver.find_element(*locator['pokedex']['advanced_search']['reset'])
        else:
            raise Exception('Invalid mouseover target given.')
        ActionChains(self.driver).move_to_element(webelement).perform()
        return
    
class ResultsList(BasePageObject):
    '''
    The ResultsList object applies to the list as a whole, not the
    results individually.
    '''
    
    def __init__(self, webdriver):
        super().__init__(webdriver)
        
    def finished_loading(self):
        '''
        Inputs: None.
        Output: <bool>
        
        Note that this method will return true even if the user has clicked the
        'load more' button but has yet to scroll down, which would
        automatically queue the next set of results to be loaded.
        '''
        # no results found
        if self.no_results_found(): return True
        # first page loaded, user has not clicked 'load more'
        if self.load_more_button_is_displayed() and not self.loading_indicator_is_displayed(): return True
        # user has clicked 'load more' and the subsequent page has finished
        #     loading
        if not self.load_more_button_is_displayed() and not self.loading_indicator_is_displayed(): return True
        return False
    
    def load_more_button_is_displayed(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        elements_list = self.driver.find_elements(*locator['pokedex']['results_list']['load_more_button'])
        if len(elements_list) == 0: return False
        button = elements_list[0]
        return button.is_displayed()
        
    def loading_indicator_is_displayed(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        elements_list = self.driver.find_elements(*locator['pokedex']['results_list']['loading_indicator'])
        if len(elements_list) == 0: return False
        indicator = elements_list[0]
        return indicator.is_displayed()
        
    def no_results_found(self):
        '''
        Inputs: None.
        Output: <bool>
        '''
        return self.driver.find_element(*locator['pokedex']['results_list']['no_results']).is_displayed()
    
    def results_found(self):
        '''
        Inputs: None.
        Output: <bool>
        
        Returns True if at least one result is found.
        '''
        if len(self.driver.find_elements(*locator['pokedex']['results_list']['index_card'])) > 0: return True
        return False
    
    def wait_to_finish_loading(self, time_limit=5.0):
        """
        Inputs: <int/float> (Optional) Wait time limit.
        Output: None if successful. Exception otherwise.
        
        This method will loop every 0.5 seconds. However, because the loop
        contains selenium find commands, and because they themselves may not
        return immediately, this method may not return immediately.
        
        It will function properly, just not necessarily with high performance. 
        """
        end_time = time.time() + time_limit
        while time.time() < end_time:
            time.sleep(0.5)
            if self.finished_loading():
                return
        raise Exception('Timeout: results list did not finish loading.')
        
    def load_next_page(self):
        '''
        Inputs: None.
        Output: <bool>
                    True if a new page was loaded.
                    False if not (all results have been loaded). 
        '''
        number_of_results_before = len(self.find_index_card_elements())
        if self.load_more_button_is_displayed():
            self.driver.find_element(*locator['pokedex']['results_list']['load_more_button']).location_once_scrolled_into_view
            self.driver.find_element(*locator['pokedex']['results_list']['load_more_button']).click()
            self.wait_to_finish_loading()
        else:
            # Scroll to the bottom of the page.
            footer = self.driver.find_element(*locator['pokedex']['footer'])
            footer.location_once_scrolled_into_view
            self.wait_to_finish_loading()
        number_of_results_after = len(self.find_index_card_elements())
        if number_of_results_before != number_of_results_after: return True
        return False
    
    def load_all(self):
        '''
        Inputs: None.
        Output: None.
        '''
        while self.load_next_page() == True: pass
        return
        
    def find_index_card_elements(self):
        '''
        Inputs: None.
        Output: <list> of <WebElements> corresponding to each individual
                result.
        '''
        return self.driver.find_elements(*locator['pokedex']['results_list']['index_card'])
    
    def find_index_card_by_name(self, value):
        '''
        Input:  <str> Pokemon's name (case-insensitive).
        Output: <WebElement>
                <None> if no match.
        '''
        index_card_list = self.find_index_card_elements()
        for index_card in index_card_list:
            if index_card.find_element(*locator['pokedex']['index_card']['name']).text.lower() == value.lower(): return index_card
        return None
    
    def find_index_card_by_number(self, value):
        '''
        Input:  <str> Pokemon's 3-digit number. Include trailing zeroes if
                      applicable.
        Output: <WebElement>
                <None> if no match.
        '''
        if type(value) is not str: raise Exception('Value must be a <str> (yes, even though searching by number).')
        if not value.startswith('#'): value = '#' + value
        if len(value) != 4: raise Exception('Number needs to be 3 digits long.')

        index_card_list = self.find_index_card_elements()
        for index_card in index_card_list:
            if index_card.find_element(*locator['pokedex']['index_card']['number']).text == value: return index_card
        return None
    
    @property
    def all_names(self):
        '''
        (Read-only) <list> of <str> All Pokemon names, in order of display.
        '''
        webelement_list = self.find_index_card_elements()
        name_list = []
        for webelement in webelement_list:
            name_list.append(webelement.find_element(*locator['pokedex']['index_card']['name']).text)
        return name_list
    
    @property
    def all_numbers(self):
        '''
        (Read-only) <list> of <str> All Pokemon numbers, in order of display.
        '''
        webelement_list = self.find_index_card_elements()
        num_list = []
        for webelement in webelement_list:
            num_list.append(webelement.find_element(*locator['pokedex']['index_card']['number']).text)
        return num_list
    
class IndexCard(BasePageObject):
    '''
    A rolodex's individual entries are index cards. Yes, I'm saying -dex
    refers to 'rolodex,' not 'index.' Don't judge me.
    '''
    
    def __init__(self, webdriver, description):
        '''
        Inputs: <WebDriver>
                <str> a Pokemon's name (case-insensitive) or its number.
                e.g.'squirtle' or '#007'. Numbers must be three digits and
                may be prefixed with '#'.
                
        Because there are multiple Index Cards on the page, one needs to be
        specified when this class is instantiated.
        '''
        super().__init__(webdriver)
        # Validate description.
        self.description = description
        try:
            int(self.description)
        except:
            # Find by number (starts with '#').
            if self.description.startswith('#'):
                if len(self.description) != 4: raise Exception('Finding an Index Card by number - number must be 3 digits.')
                self.find_webelement_by = 'number'
                return
            # Find by name.
            else:
                self.find_webelement_by = 'name'
                return
        else:
            # Find by number (does not start with '#').
            if len(self.description) != 3: raise Exception('Finding an Index Card by number - number must be 3 digits.')
            self.find_webelement_by = 'number'
            return
        
    @property
    def webelement(self):
        '''
        (Read-only) <WebElement> corresponding to description given when
        the class is instantiated (see __init__).
        '''
        # Find WebElement corresponding to its description.
        results_list = ResultsList(self.driver)
        if self.find_webelement_by == 'name': webelement = results_list.find_index_card_by_name(self.description)
        else: webelement = results_list.find_index_card_by_number(self.description)
        if webelement == None: raise Exception("No Index Card found with description '{}'.".format(self.description))
        return webelement
    
    @property
    def name(self):
        '''
        (Read-only) <str> Pokemon's name. 
        '''
        return self.webelement.find_element(*locator['pokedex']['index_card']['name']).text
    
    @property
    def number(self):
        '''
        (Read-only) <str> Pokemon's number.
        '''
        return self.webelement.find_element(*locator['pokedex']['index_card']['number']).text
        
    @property
    def types(self):
        '''
        (Read-only) <list> of <str>, e.g. ['Grass', 'Poison']
        '''
        str_list = []
        webelement_list = self.webelement.find_elements(*locator['pokedex']['index_card']['types'])
        for webelement in webelement_list: str_list.append(webelement.text)
        return str_list
    