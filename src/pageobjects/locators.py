from selenium.webdriver.common.by import By

locator = {}
locator['pokedex'] = {}
locator['pokedex']['search'] = {}
locator['pokedex']['search']['field'] = (By.ID, 'searchInput')
locator['pokedex']['search']['dropdown'] = (By.CSS_SELECTOR, 'div.tt-menu')
locator['pokedex']['search']['suggestion'] = (By.CSS_SELECTOR, 'div.tt-suggestion')
locator['pokedex']['search']['button'] = (By.CSS_SELECTOR, 'input[value="Search"]')
locator['pokedex']['advanced_search'] = {}
locator['pokedex']['advanced_search']['toggle'] = (By.CSS_SELECTOR, 'div.filter-toggle-span > span')
locator['pokedex']['advanced_search']['filters'] = (By.CSS_SELECTOR, 'div.pokedex-filter-wrapper')
locator['pokedex']['advanced_search']['tw'] = (By.CSS_SELECTOR, 'ul.pokedex-filter-tw-list > li')
locator['pokedex']['advanced_search']['height_short'] = (By.CSS_SELECTOR, 'li[data-type="height"][data-value="short"]')
locator['pokedex']['advanced_search']['height_medium'] = (By.CSS_SELECTOR, 'li[data-type="height"][data-value="medium"]')
locator['pokedex']['advanced_search']['height_tall'] = (By.CSS_SELECTOR, 'li[data-type="height"][data-value="tall"]')
locator['pokedex']['advanced_search']['weight_light'] = (By.CSS_SELECTOR, 'li[data-type="weight"][data-value="light"]')
locator['pokedex']['advanced_search']['weight_medium'] = (By.CSS_SELECTOR, 'li[data-type="weight"][data-value="medium"]')
locator['pokedex']['advanced_search']['weight_heavy'] = (By.CSS_SELECTOR, 'li[data-type="weight"][data-value="heavy"]')
locator['pokedex']['advanced_search']['search'] = (By.CSS_SELECTOR, 'a[id="search"]') #ugh there are two elements with ID=search
locator['pokedex']['advanced_search']['reset'] = (By.ID, 'reset')
locator['pokedex']['tw_filter'] = {}
locator['pokedex']['tw_filter']['description'] = (By.CSS_SELECTOR, 'span.pill')
locator['pokedex']['tw_filter']['type_toggle'] = (By.CSS_SELECTOR, 'span.type')
locator['pokedex']['tw_filter']['weakness_toggle'] = (By.CSS_SELECTOR, 'span.weakness')
locator['pokedex']['sort'] = {}
locator['pokedex']['sort']['button'] = (By.CSS_SELECTOR, 'label.styled-select')
locator['pokedex']['sort']['dropdown'] = (By.CSS_SELECTOR, 'div.custom-select-menu > ul')
locator['pokedex']['sort']['dropdown_options'] = (By.CSS_SELECTOR, 'li')
locator['pokedex']['results_list'] = {}
locator['pokedex']['results_list']['no_results'] = (By.CSS_SELECTOR, 'div.no-results')
locator['pokedex']['results_list']['index_card'] = (By.CSS_SELECTOR, 'li.animating')
locator['pokedex']['results_list']['loading_indicator'] = (By.CSS_SELECTOR, 'div.loader')
locator['pokedex']['results_list']['load_more_button'] = (By.XPATH, "//span[contains(.,'Load more Pokémon')]")
locator['pokedex']['index_card'] = {}
locator['pokedex']['index_card']['name'] = (By.CSS_SELECTOR, 'div.pokemon-info > h5')
locator['pokedex']['index_card']['number'] = (By.CSS_SELECTOR, 'div.pokemon-info > p.id')
locator['pokedex']['index_card']['types'] = (By.CSS_SELECTOR, 'div.abilities > span')
locator['pokedex']['footer'] = (By.CSS_SELECTOR, 'footer')
