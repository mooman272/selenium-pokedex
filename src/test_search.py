import unittest

from selenium import webdriver

from pageobjects import *

class Search(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get('http://www.pokemon.com/us/pokedex')
        self.results = pokedex.ResultsList(self.driver)
        self.results.wait_to_finish_loading()
        self.search = pokedex.Search(self.driver)

    def test_query_remains_after_search(self):
        self.search.search_for('meowth')
        assert self.search.query == 'meowth', "Search field actual value '{}'; expected 'meowth'.".format(self.search.query)
    
    def test_search_dropdown_appears(self):
        self.search.query = 'ca'
        assert self.search.dropdown_is_displayed(), 'Dropdown did not appear.'
        
    def test_search_button_mouseover(self):
        button_color = self.driver.find_element(*locators.locator['pokedex']['search']['button']).value_of_css_property('background-color')
        assert button_color == 'rgba(238, 107, 47, 1)'
        self.search.mouseover('search_button')
        button_color = self.driver.find_element(*locators.locator['pokedex']['search']['button']).value_of_css_property('background-color')
        assert button_color == 'rgba(218, 71, 27, 1)'

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()