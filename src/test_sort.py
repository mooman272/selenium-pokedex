import unittest

from selenium import webdriver

from pageobjects import *

class Sort(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get('http://www.pokemon.com/us/pokedex')
        self.results = pokedex.ResultsList(self.driver)
        self.results.wait_to_finish_loading()
        self.sort = pokedex.Sort(self.driver)

    def test_sort_by_lowest_number(self):
        self.sort.by('lowest number (first)')
        assert self.sort.verify_sort(), 'Results not sorted properly.'

    def test_sort_by_highest_number(self):
        self.sort.by('highest number (first)')
        assert self.sort.verify_sort(), 'Results not sorted properly.'

    def test_sort_by_az(self):
        self.sort.by('a-z')
        assert self.sort.verify_sort(), 'Results not sorted properly.'

    def test_sort_by_za(self):
        self.sort.by('z-a')
        assert self.sort.verify_sort(), 'Results not sorted properly.'
        
    def test_sort_after_load_pages(self):
        self.results.load_next_page()
        self.results.load_next_page()
        self.sort.by('a-z')
        assert self.sort.verify_sort(), 'Results not sorted properly.'

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()