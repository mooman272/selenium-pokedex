import time
import unittest

from selenium import webdriver
from pageobjects import *

class SearchObjectTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get('http://www.pokemon.com/us/pokedex')
        self.search_field_webelement = self.driver.find_element(*locators.locator['pokedex']['search']['field'])
        time.sleep(1)
        self.search = pokedex.Search(self.driver)
        self.sort = pokedex.Sort(self.driver)
    
    def test_set_query(self):
        self.search.set_query('pika')
        assert self.search_field_webelement.get_attribute('value') == 'pika', 'Search field was not populated.'
    
    def test_search_for(self):
        # nothing really to assert; just getting past the implied exceptions will suffice
        self.search.search_for('pika')
        self.search.search_for('asdf')
        
    def test_dropdown_wait_to_open_close(self):
        # dropdown is closed - wait for it to open without doing anything
        try: self.sort.wait_for_dropdown_to_open()
        except Exception: pass
        else: assert False, 'wait_for_dropdown_to_open() did not raise an Exception.'
        # open the dropdown
        self.sort.find_element().click()
        self.sort.wait_for_dropdown_to_open()
        # now wait for it to close without doing anything
        try: self.sort.wait_for_dropdown_to_close()
        except Exception: pass
        else: assert False, 'wait_for_dropdown_to_close() did not raise an Exception.'
        
    def test_dropdown_open_close(self):
        # close the dropdown even though it's already closed
        try: self.sort.close_dropdown()
        except Exception: pass
        else: assert False, 'close_dropdown() did not raise an Exception.'
        self.sort.open_dropdown()
        # open the dropdown even though it's already open
        try: self.sort.open_dropdown()
        except Exception: pass
        else: assert False, 'open_dropdown() did not raise an Exception.'
        
    def test_dropdown_find_option(self):
        # find options when dropdown is closed
        try: self.sort.find_element_with_option('test')
        except Exception: pass
        else: assert False, 'find_element_with_option() did not raise an Exception.'
        self.sort.open_dropdown()
        # case-sensitivity
        test_string = 'Lowest Number (First)'
        assert self.sort.find_element_with_option(test_string) != None, 'Original string search returned no results.'
        assert self.sort.find_element_with_option(test_string.lower()) != None, 'lower() string search returned no results.'
        # no results
        assert self.sort.find_element_with_option('asdf') == None, 'Negative string search returned no results.'
        # ellipsis
        test_string = 'Sort results by' + chr(8230)
        assert self.sort.find_element_with_option(test_string) != None, 'Ellipsis string search returned no results.'
    
    def tearDown(self):
        self.driver.quit()
