# Unit Tests

Yes, I'm unit testing my UI testing. (Yo dawg.)

Tests in this folder are intended to test the units (read: classes and their methods) defined in the pageobjects folder. These tests are unlikely test cases a QA would write, though there may be a little overlap.

Actual UI tests should go into the /src folder.

## Execution

Because these tests and their structures rely on relative inputs, attempting to execute them the standard way ('python <filename>.py') from this directory will result in an import error.

Perform the follwing steps:

1. Go to the /src/ directory.
2. Execute python -m <unit test tool of your choice> unittests.<test module>
    * e.g. python -m unittest unittests.test\_search_class  
    * e.g. python -m py.test unittests/test\_search_class.py  
    Note that these syntaxes are correct. unittest (python built-in module) uses dots for directory location while pytest (3rd party module) uses slashes. Also note that unittest will not work if you include .py file extension, whereas pytest requires it.

You can also run these tests via Eclipse / PyDev as a unittest. I didn't have to make any edits to the PyUnit test runner.
